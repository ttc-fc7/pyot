
Test scripts description:


SetupOT.sh

- enable trigger accept
- enable TTS FSM
- switch to AMC13 clock source

SetupStubTrigger.sh

fast command processor:
- select user trigger source
- accept all triggers
- turn off backpressure
- load configuration
- start sending triggers
- check number of triggers

