
echo "Turning on continuous triggers in the FC7 with IP $1"
echo "Note: to turn them off, you need to reconfigure the fast command block"

# Select user trigger source in fast command processor
ot "$1" fcp set trg_src 3

# Accept all triggers in fast command processor
ot "$1" fcp accept 0

# Turn off backpressure in fast command processor
ot "$1" fcp set bp 0

# Load fast command processor configuration
ot "$1" fcp load_config

# Start sending triggers in fast command processor
ot "$1" fcp start

# Check number of triggers in fast command processor
ot "$1" fcp status
