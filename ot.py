#!/usr/bin/env python
#----------------------------------------------------------------------------------
# file ttc.py
#----------------------------------------------------------------------------------
# author : Viktor Veszpremi (viktor.veszpremi@cern.ch)
# contributions from:
#          Tamas Balazs (tamas.balazs@cern.ch)
#          Lajos Palanki (LP618@ic.ac.uk)
#          Alexander Ruede (aruede@cern.ch)
#----------------------------------------------------------------------------------
# version 1.0
#
# Details :
# TTC-FC7 firmware python interface
#
# Last changes:
# 2021.09.07
#---------------------------------------------------------------------------------

import sys
from pyOT import *
from time import sleep
import math
import os

ip = os.getenv("IPOT")
uDTC = pyOT(ip)
sleep(0.2)


################################################################
### HELP
################################################################

if (len(sys.argv) > 1 and sys.argv[1] == "help") or (len(sys.argv)==1):
    print ("Main commands")
    print ("    reset\t: command issues general reset")
    print ("    clk_amc13\t: switch default clock source of OT-FC7 to AMC13")
    print ("    configure\t: configure OT-FC7 for accepting triggers from TTC-FC7")
    print (" ")
    print ("Main command groups")
    print ("    cnc\t\t: clock and counters")
    print ("    fmc\t\t: powering fmc")
    print ("    ttc\t\t: configure TTC decoder")
    print ("    fcp\t\t: configure fast command processor")
    print ("    tts\t\t: read TTS state and test TTS FSM")
    print (" ")
    print ("Note: running command group without a command prints")
    print ("out a help for the command group ")
    print (" ")

################################################################
### Configure hardware
################################################################

# General reset of the user_core
if len(sys.argv) > 1 and sys.argv[1] == "reset":
    uDTC.reset_setup()

if len(sys.argv) > 1 and sys.argv[1] == "clk_amc13":
    # switch to AMC13 clock source
    uDTC.u7_out_3(0)
    sleep(0.2)
    uDTC.reset_setup()
    sleep(0.2)
    uDTC.status_clk_gen()

if len(sys.argv) > 1 and sys.argv[1] == "configure":
    # switch to AMC13 clock source
    uDTC.u7_out_3(0)
    sleep(0.2)
    uDTC.reset_setup()
    sleep(0.2)
    uDTC.status_clk_gen()
    # enable TTS FSM
    uDTC.set_tts_fsm_enable()
    uDTC.set_ipbus_any_buffer_full(0)
    uDTC.set_ipbus_all_buffer_empty(0)
    # configure TTC decoder
    uDTC.reset_TTC_dec()
    sleep(1)
    uDTC.enable_ttc_dec()
    # configure fast command processor
    uDTC.reset_fast_command()
    sleep(1)
    uDTC.set_trigger_source(1)
    uDTC.set_triggers_to_accept()
    uDTC.set_backpressure(0)
    uDTC.set_load_configure()
    sleep(1)
    uDTC.set_start_trigger()
    sleep(0.5)
    uDTC.status_fcp()


################################################################
### Clocks and Counters
################################################################

if len(sys.argv) == 2 and sys.argv[1] == "cnc":
    print ("Commands")
    print ("    cnt\t\t : print out incidental values of counters")
    print ("    clk_locked\t : print out status of clock generators")
    print ("    clk_src_stat : print out clock configuration")
    print ("    set ...")
    print ("        ext_clk\t : 0 = on-board clock source, 1 = external clock")
    print ("        u8_out_1 : 3 = internal 40.08 MHz oscillator, 2 = fmc_l8_clk1, 1 = fmc_l8_clk0, 0 = coax input (J5, J6)")
    print ("        U8_out_4 : 1 = internal 40.08 MHz oscillator, 0 = coax input (J5, J6)")
    print ("        u7_out_1 : 3 = [U8 OUT1], 2 = TCLKA (backplane), 1 = TCLKC, 0 = AMC13")
    print ("        u7_out_2 : 3 = [U8 OUT1], 2 = TCLKA (backplane), 1 = TCLKC, 0 = AMC13")
    print ("        u7_out_3 : 3 = [U8 OUT1], 2 = TCLKA (backplane), 1 = TCLKC, 0 = AMC13")
    print ("        u7_out_4 : 3 = [U8 OUT1], 2 = TCLKA (backplane), 1 = TCLKC, 0 = AMC13")
    print (" ")
    print ("Note: default configuration ")
    print ("      internal 40.08 MHz oscillator --> U8 IN4 - OUT1 --> U7 IN4 - OUT3 --> FABRIC_CLK  ")
    print (" ")
    print ("Expert commands")
    print ("    ctrl_regs_stat\t : print out ctrl_regs 1 system registers")
    print (" ")
    
#  Reading clock rate tool status
if len(sys.argv) > 2 and sys.argv[1] == "cnc" and sys.argv[2] == "clk_locked":
    uDTC.status_clk_gen()

#  Reading counter values
if len(sys.argv) > 2 and sys.argv[1] == "cnc" and sys.argv[2] == "cnt":
    uDTC.status_cnt()

if len(sys.argv) > 4 and sys.argv[1] == "cnc" and sys.argv[2] == "set" and sys.argv[3] == "ext_clk":
    uDTC.ext_clk(int(sys.argv[4]))
    sleep(0.2)
    uDTC.reset_setup()
    sleep(0.2)
    uDTC.status_clk_gen()

if len(sys.argv) > 4 and sys.argv[1] == "cnc" and sys.argv[2] == "set" and sys.argv[3] == "u8_out_1":
    uDTC.u8_out_1(int(sys.argv[4]))
    sleep(0.2)
    uDTC.reset_setup()
    sleep(0.2)
    uDTC.status_clk_gen()
    
if len(sys.argv) > 4 and sys.argv[1] == "cnc" and sys.argv[2] == "set" and sys.argv[3] == "u8_out_4":
    uDTC.u8_out_4(int(sys.argv[4]))
    sleep(0.2)
    uDTC.reset_setup()
    sleep(0.2)
    uDTC.status_clk_gen()

if len(sys.argv) > 4 and sys.argv[1] == "cnc" and sys.argv[2] == "set" and sys.argv[3] == "u7_out_1":
    uDTC.u7_out_1(int(sys.argv[4]))
    sleep(0.2)
    uDTC.reset_setup()
    sleep(0.2)
    uDTC.status_clk_gen()
    
if len(sys.argv) > 4 and sys.argv[1] == "cnc" and sys.argv[2] == "set" and sys.argv[3] == "u7_out_2":
    uDTC.u7_out_2(int(sys.argv[4]))
    sleep(0.2)
    uDTC.reset_setup()
    sleep(0.2)
    uDTC.status_clk_gen()
    
if len(sys.argv) > 4 and sys.argv[1] == "cnc" and sys.argv[2] == "set" and sys.argv[3] == "u7_out_3":
    uDTC.u7_out_3(int(sys.argv[4]))
    sleep(0.2)
    uDTC.reset_setup()
    sleep(0.2)
    uDTC.status_clk_gen()

if len(sys.argv) > 4 and sys.argv[1] == "cnc" and sys.argv[2] == "set" and sys.argv[3] == "u7_out_4":
    uDTC.u7_out_4(int(sys.argv[4]))
    sleep(0.2)
    uDTC.reset_setup()
    sleep(0.2)
    uDTC.status_clk_gen()
    
#  Reading clock sources
if len(sys.argv) > 2 and sys.argv[1] == "cnc" and sys.argv[2] == "clk_src_stat":
    uDTC.status_clk_src()

#  Reading ctrl registers
if len(sys.argv) > 2 and sys.argv[1] == "cnc" and sys.argv[2] == "ctrl_regs_stat":
    uDTC.status_ctrl_regs()

################################################################
### Powering FMC
################################################################

if len(sys.argv) == 2 and sys.argv[1] == "fmc":
    print ("Commands")
    print ("    on \t\t: turns power ON for both L8 and L12 FMC")
    print ("    off \t: turns power OFF for both L8 and L12 FMC")
    print (" ")
    print ("Expert commands")
    print ("    l12_pwr_on \t: turns power ON for L12 FMC")
    print ("    l8_pwr_on \t: turns power ON for L8 FMC")
    print (" ")
    print ("Note: fmc power on is called during configure, no need to call it again")
    print (" ")
    
if len(sys.argv) > 2 and sys.argv[1] == "fmc" and sys.argv[2] == "l12_pwr_on":
    uDTC.l12_pwr_on()

if len(sys.argv) > 2 and sys.argv[1] == "fmc" and sys.argv[2] == "l8_pwr_on":
    uDTC.l8_pwr_on()

if len(sys.argv) > 2 and sys.argv[1] == "fmc" and sys.argv[2] == "on":
    uDTC.fmc_pwr_on()

if len(sys.argv) > 2 and sys.argv[1] == "fmc" and sys.argv[2] == "off":
    uDTC.fmc_pwr_off()

################################################################
### TTC Decoder
################################################################

if len(sys.argv) == 2 and sys.argv[1] == "ttc":
    print ("TTC decoder commands")
    print ("    reset\t: reset TTC decoder")
    print ("    enable\t: enable TTC decoder")
    print ("    status\t: TTC decoder status")
    print (" ")
    print ("Note: TTC decoder configuration is called during main configure, no need to call it again")
    print (" ")

if len(sys.argv) > 2 and sys.argv[1] == "ttc" and sys.argv[2] == "reset":
    uDTC.reset_TTC_dec()

if len(sys.argv) > 2 and sys.argv[1] == "ttc" and sys.argv[2] == "enable":
    uDTC.enable_ttc_dec()

if len(sys.argv) > 2 and sys.argv[1] == "ttc" and sys.argv[2] == "status":
    uDTC.status_ttc()
    
################################################################
### Fast Command Processor
################################################################

if len(sys.argv) == 2 and sys.argv[1] == "fcp":
    print ("Fast Command Processor commands")
    print ("    reset\t: reset Fast Command Processor")
    print ("    set ...")
    print ("        trg_src\t: trigger source: 1 - TTC, 2 - Stubs, 3 - User Trigger, 4 - TLU, 5 - External trigger,")
    print ("                                  6 - test pulse FSM, 7 - UIB antenna FSM, 8 - Consecutive triggers FSM")
    print ("    accept\t: number of triggers to accept (0 - continuous triggering)")
    print ("    set ...")
    print ("        bp\t: backpressure 0: OFF, 1: ON")
    print ("    load_config\t: configure Fast Command Processor")
    print ("    start\t: start triggering")
    print ("    status\t: configured, source, fsm state, counter ")
    print (" ")
    print ("Note: Fast Command Processor configuration is called during main configure, no need to call it again")
    print (" ")

if len(sys.argv) > 2 and sys.argv[1] == "fcp" and sys.argv[2] == "reset":
    uDTC.reset_fast_command()

if len(sys.argv) > 4 and sys.argv[1] == "fcp" and sys.argv[2] == "set" and sys.argv[3] == "trg_src":
    uDTC.set_trigger_source(int(sys.argv[4]))

if len(sys.argv) > 2 and sys.argv[1] == "fcp" and sys.argv[2] == "accept":
    uDTC.set_triggers_to_accept()

if len(sys.argv) > 4 and sys.argv[1] == "fcp" and sys.argv[2] == "set" and sys.argv[3] == "bp":
    uDTC.set_backpressure(int(sys.argv[4]))

if len(sys.argv) > 2 and sys.argv[1] == "fcp" and sys.argv[2] == "load_config":
    uDTC.set_load_configure()

if len(sys.argv) > 2 and sys.argv[1] == "fcp" and sys.argv[2] == "start":
    uDTC.set_start_trigger()

if len(sys.argv) > 2 and sys.argv[1] == "fcp" and sys.argv[2] == "status":
    uDTC.status_fcp()

################################################################
### TTS decoder and FSM commands
################################################################

if len(sys.argv) == 2 and sys.argv[1] == "tts":
    print ("Commands")
    print ("    enable_fsm\t : enable TTS FSM reacting to TTS input from AMC13")
    print ("    disable_fsm\t : disable TTS FSM reacting to TTS input from AMC13, ipbus defines actual TTS state")
    print ("    state\t : print value of sw TTS state")
    print (" ")
    print ("Expert commands")
    print ("    set")
    print ("        tts <value>\t : set actual TTS state via ipbus")
    print ("        ipbus_any_buffer_full <value>\t : set any buffer full bit via ipbus")
    print ("        ipbus_all_buffer_empty <value>\t : set all buffer empty bit via ipbus")
    print (" ")

if len(sys.argv) > 2 and sys.argv[1] == "tts" and sys.argv[2] == "enable_fsm":
    uDTC.set_tts_fsm_enable()

if len(sys.argv) > 2 and sys.argv[1] == "tts" and sys.argv[2] == "disable_fsm":
    uDTC.set_tts_fsm_disable()

if len(sys.argv) > 2 and sys.argv[1] == "tts" and sys.argv[2] == "state":
    uDTC.read_tts_state()

if len(sys.argv) > 4 and sys.argv[1] == "tts" and sys.argv[2] == "set" and sys.argv[3] == "tts":
    uDTC.set_tts_state(int(sys.argv[4]))

if len(sys.argv) > 4 and sys.argv[1] == "tts" and sys.argv[2] == "set" and sys.argv[3] == "ipbus_any_buffer_full":
    uDTC.set_ipbus_any_buffer_full(int(sys.argv[4]))

if len(sys.argv) > 4 and sys.argv[1] == "tts" and sys.argv[2] == "set" and sys.argv[3] == "ipbus_all_buffer_empty":
    uDTC.set_ipbus_all_buffer_empty(int(sys.argv[4]))

################################################################################################################################
################################################################################################################################
################################################################################################################################
################################################################################################################################


#---------------------------------------------------------------------------------

#-- debug only




################################################################################################################################
################################################################################################################################
################################################################################################################################
################################################################################################################################

#print " "
#print "*** Error: does not compute"
#print " "
