#----------------------------------------------------------------------------------
# file pyTTC.py
#----------------------------------------------------------------------------------
# author : Viktor Veszpremi (viktor.veszpremi@cern.ch)
# contributions from:
#          Tamas Balazs (tamas.balazs@cern.ch)
#          Lajos Palanki (LP618@ic.ac.uk)
#          Alexander Ruede (aruede@cern.ch)
#----------------------------------------------------------------------------------
# version 1.0
#
# Details :
# Library for TTC-FC7 firmware python interface
#
# Last changes:
# 2021.09.07
#---------------------------------------------------------------------------------

import sys
import uhal
from time import sleep

class pyOT:

    def __init__(self, ipaddr):
        uhal.setLogLevelTo(uhal.LogLevel.WARNING)
        self.ipaddr = ipaddr
	# udp transactions worked before CentOS 7.7 upgrade only !
	# self.hw = uhal.getDevice("FC7", "ipbusudp-2.0://" + self.ipaddr + ":50001", "file://./cfg/device_address_table_fc7.xml")
        self.hw = uhal.getDevice("FC7", "chtcp-2.0://localhost:10203?target=" + self.ipaddr + ":50001", "file://./cfg/device_address_table_fc7.xml")

################################################################
### IPbus
################################################################

    def ipb_read(self, nodeID, dispatch=1):
        word = self.hw.getNode(nodeID).read()
        if dispatch == 1:
        	self.hw.dispatch()
        return int(word)

    def ipb_write(self, nodeID, data, dispatch=1):
        self.hw.getNode(nodeID).write(data)
        if dispatch == 1:
        	self.hw.dispatch()
        return int(data)

    def ipb_dispatch(self):
        return self.hw.dispatch()

################################################################
### General config
################################################################
    
    # switching clock source in user_core clock generator with MUX to fmc_l8_clk1 external clock DIO5 channel 5
    def ext_clk(self, source):
        self.ipb_write("usr.cnfg.clock.ext_clk_en",source,1)
        print("Clock source switched to:" + bin(source))

    # switching u8 out 1 
    def u8_out_1(self, source):
        self.ipb_write("sys.ctrl.clk_ctrl.k7_master_xpoint_ctrl_8_9",source,1)
        print("Clock source switched to:" + bin(source))

    # switching u8 out 4
    def u8_out_4(self, source):
        self.ipb_write("sys.ctrl.clk_ctrl.osc_coax_sel",source,1)
        print("Clock source switched to:" + bin(source))

    # switching u7 out 1 
    def u7_out_1(self, source):
        self.ipb_write("sys.ctrl.clk_ctrl.k7_master_xpoint_ctrl_0_1",source,1)
        print("Clock source switched to:" + bin(source))

    # switching u7 out 2 
    def u7_out_2(self, source):
        self.ipb_write("sys.ctrl.clk_ctrl.k7_master_xpoint_ctrl_2_3",source,1)
        print("Clock source switched to:" + bin(source))

    # switching u7 out 3 
    def u7_out_3(self, source):
        self.ipb_write("sys.ctrl.clk_ctrl.k7_master_xpoint_ctrl_4_5",source,1)
        print("Clock source switched to:" + bin(source))

    # switching u7 out 4
    def u7_out_4(self, source):
        self.ipb_write("sys.ctrl.clk_ctrl.k7_master_xpoint_ctrl_6_7",source,1)
        print("Clock source switched to:" + bin(source))

    # reading status of clock sources
    def status_clk_src(self):
        ext_clk = self.ipb_read("usr.cnfg.clock.ext_clk_en",1)
        clock_source_u8_OUT1_sel = self.ipb_read("sys.ctrl.clk_ctrl.k7_master_xpoint_ctrl_8_9",1)
        clock_source_u8_OUT4_sel = self.ipb_read("sys.ctrl.clk_ctrl.osc_coax_sel",1)
        clock_source_u7_OUT1_sel = self.ipb_read("sys.ctrl.clk_ctrl.k7_master_xpoint_ctrl_0_1",1)
        clock_source_u7_OUT2_sel = self.ipb_read("sys.ctrl.clk_ctrl.k7_master_xpoint_ctrl_2_3",1)
        clock_source_u7_OUT3_sel = self.ipb_read("sys.ctrl.clk_ctrl.k7_master_xpoint_ctrl_4_5",1)
        clock_source_u7_OUT4_sel = self.ipb_read("sys.ctrl.clk_ctrl.k7_master_xpoint_ctrl_6_7",1)
        print ("------------------------------------------------")
        print ("MUX in clock generator")        
        print ("ext_clk           ", ext_clk,                  "  --> 0 = fabric clock, 1 external clock DIO5 channel 5")
        print ("")
        print ("U8 XPOINT switch")  
        print ("OUT1 select       ", clock_source_u8_OUT1_sel, "  --> 3 = internal 40.08 MHz oscillator, 2 = fmc_l8_clk1, 1 = fmc_l8_clk0, 0 = coax input (J5, J6)")
        print ("OUT2 select            --> FMC_L8_CLK0 hard wired !")
        print ("OUT3 select            --> FMC_L8_CLK1 hard wired !")
        print ("OUT4 select       ", clock_source_u8_OUT4_sel, "  --> 1 = internal 40.08 MHz oscillator, 0 = coax input (J5, J6)")
        print ("")
        print ("U7 XPOINT switch")  
        print ("OUT1 select       ", clock_source_u7_OUT1_sel, "  --> 3 = [U8 OUT1], 2 = TCLKA (backplane), 1 = TCLKC, 0 = AMC13")
        print ("OUT2 select       ", clock_source_u7_OUT2_sel, "  --> 3 = [U8 OUT1], 2 = TCLKA (backplane), 1 = TCLKC, 0 = AMC13")
        print ("OUT3 select       ", clock_source_u7_OUT3_sel, "  --> 3 = [U8 OUT1], 2 = TCLKA (backplane), 1 = TCLKC, 0 = AMC13")
        print ("OUT4 select       ", clock_source_u7_OUT4_sel, "  --> 3 = [U8 OUT1], 2 = TCLKA (backplane), 1 = TCLKC, 0 = AMC13")
        print ("------------------------------------------------")
        print ("")
    
    # reading status of clock sources
    def status_ctrl_regs(self):
        ctrl_regs = self.ipb_read("sys.ctrl.clk_ctrl",1)
        print ("------------------------------------------------")
        print ("ctrl_regs         ", hex(ctrl_regs))
        print ("------------------------------------------------")
        print ("")
                        
    # reading status of clock generators
    def status_clk_gen(self):
        clk_40_locked = self.ipb_read("usr.stat.general.clock_generator.clk_40_locked",1)
        ref_clk_locked  = self.ipb_read("usr.stat.general.clock_generator.ref_clk_locked",1)
        print ("------------------------------------------------")
        print ("clk_40_locked     ", clk_40_locked)
        print ("ref_clk_locked    ", ref_clk_locked)
        print ("------------------------------------------------")
        print ("")

    # reading power status of FMC connectors 
    def status_fmc_pwr(self):
        l12_pwr_en = self.ipb_read("sys.ctrl.fmc_pwr.l12_pwr_en",1)
        l8_pwr_en  = self.ipb_read("sys.ctrl.fmc_pwr.l8_pwr_en",1)
        pg_c2m = self.ipb_read("sys.ctrl.fmc_pwr.pg_c2m",1)
        print ("------------------------------------------------")
        print ("l12_pwr_en        ", l12_pwr_en)
        print ("l8_pwr_en         ", l8_pwr_en)
        print ("pg_c2m            ", pg_c2m)
        print ("------------------------------------------------")
        print ("")

    # reading counter values
    def status_cnt(self):
        evcnt = self.ipb_read("usr.stat.be_proc.general.evnt_cnt",1)
        #bcnt = self.ipb_read("usr.stat_regs.counter_1.bcnt",1)
        #orbcnt = self.ipb_read("usr.stat_regs.counter_2.orbcnt",1)
        #evrcnt = self.ipb_read("usr.stat_regs.counter_2.evrcnt",1)
        print ("Counter values ...")
        print ("------------------------------------------------")
        print ("event counter             ", evcnt)
        print ("bunch crossing counter    ", 0)
        print ("orbit counter             ", 0)
        print ("event reject counter      ", 0)
        print ("------------------------------------------------")
        print ("")

################################################################
### Powering FMC
################################################################
    
    def l12_pwr_on(self):
        self.ipb_write("sys.ctrl.fmc_pwr.l12_pwr_en",1,1)
        self.ipb_write("sys.ctrl.fmc_pwr.pg_c2m",1,1)
        print('L12 FMC power ON')
        self.status_fmc_pwr()

    def l8_pwr_on(self):
        self.ipb_write("sys.ctrl.fmc_pwr.l8_pwr_en",1,1)
        self.ipb_write("sys.ctrl.fmc_pwr.pg_c2m",1,1)
        print('L8 FMC power ON')
        self.status_fmc_pwr()

    def fmc_pwr_on(self):
        self.ipb_write("sys.ctrl.fmc_pwr.l12_pwr_en",1,1)
        self.ipb_write("sys.ctrl.fmc_pwr.l8_pwr_en",1,1)
        self.ipb_write("sys.ctrl.fmc_pwr.pg_c2m",1,1)
        print('L8 & L12 FMC power ON')
        self.status_fmc_pwr()

    def fmc_pwr_off(self):
        self.ipb_write("sys.ctrl.fmc_pwr.l12_pwr_en",0,1)
        self.ipb_write("sys.ctrl.fmc_pwr.l8_pwr_en",0,1)
        self.ipb_write("sys.ctrl.fmc_pwr.pg_c2m",0,1)
        print('L8 & L12 FMC power OFF')
        self.status_fmc_pwr()

    def set_reset_all(self):
        self.ipb_write("usr.ctrl.command_processor_block.global.reset",1,1)
        self.ipb_write("usr.ctrl.global.clock_generator.clk40_reset",1,1)
        self.ipb_write("usr.ctrl.global.clock_generator.refclk_reset",1,1)

    def release_reset_all(self):
        self.ipb_write("usr.ctrl.command_processor_block.global.reset",0,1)
        self.ipb_write("usr.ctrl.global.clock_generator.clk40_reset",0,1)
        self.ipb_write("usr.ctrl.global.clock_generator.refclk_reset",0,1)
        sleep(0.4)

    def reset_setup(self):
        print ("Resetting...")
        self.set_reset_all()
        sleep(0.2)
        self.release_reset_all()
        sleep(0.2)
        print("Reset DONE.")
        #sys.exit(1)

################################################################
### TTC decoder
################################################################

    def set_ttc_dec_reset(self):
        self.ipb_write("usr.ctrl.global.ttc.dec_reset",1,1)

    def release_ttc_dec_reset(self):
        self.ipb_write("usr.ctrl.global.ttc.dec_reset",0,1)
        sleep(0.4)

    def reset_TTC_dec(self):
        print ("Resetting TTC decoder...")
        self.set_ttc_dec_reset()
        sleep(0.2)
        self.release_ttc_dec_reset()
        sleep(0.2)
        print("Reset DONE.")
        #sys.exit(1)

    def enable_ttc_dec(self):
        self.ipb_write("usr.cnfg.ttc.ttc_enable",1,1)
        sleep(0.4)
        TTC_dec_enable = self.ipb_read("usr.cnfg.ttc.ttc_enable",1)
        print ("TTC decoder state: ", TTC_dec_enable)

    def status_ttc(self):
        ready = self.ipb_read("usr.stat.general.ttc.ready",1)
        single_error_count = self.ipb_read("usr.stat.general.ttc.dec_err_cnt.single",1)
        double_error_count = self.ipb_read("usr.stat.general.ttc.dec_err_cnt.double",1)
        print ("TTC decoder status ...")
        print ("------------------------------------------------")
        print ("ready                     ", ready)
        print ("single_error_count        ", single_error_count)
        print ("double_error_count        ", double_error_count)
        print ("------------------------------------------------")
        print ("")

################################################################
### Fast Command Processor
################################################################

    def set_fast_command_reset(self):
        self.ipb_write("usr.ctrl.fast_command_block.control.reset",1,1)

    def release_fast_command_reset(self):
        self.ipb_write("usr.ctrl.fast_command_block.control.reset",0,1)
        sleep(0.4)

    def reset_fast_command(self):
        print ("Resetting Fast Command Processor...")
        self.set_fast_command_reset()
        sleep(0.2)
        self.release_fast_command_reset()
        sleep(0.2)
        print("Reset DONE.")
        #sys.exit(1)

    def set_trigger_source(self, source):
        self.ipb_write("usr.cnfg.fast_command_block.trigger_source",source,1)
        sleep(0.4)
        trigger_source = self.ipb_read("usr.cnfg.fast_command_block.trigger_source",1)
        print ("Trigger source: ", trigger_source)

    def set_triggers_to_accept(self):
        self.ipb_write("usr.cnfg.fast_command_block.triggers_to_accept",0,1)
        triggers_to_accept = self.ipb_read("usr.cnfg.fast_command_block.triggers_to_accept",1)
        print ("Number of triggers to accept: ", triggers_to_accept)

    def set_backpressure(self, bit):
        self.ipb_write("usr.cnfg.fast_command_block.backpressure_enable",bit,1)
        sleep(0.4)
        backpressure = self.ipb_read("usr.cnfg.fast_command_block.backpressure_enable",1)
        print ("Backpressure: ", backpressure)

    def set_load_configure(self):
        self.ipb_write("usr.ctrl.fast_command_block.control.load_config",1,1)
        configured = self.ipb_read("usr.stat.fast_command_block.general.configured",1)
        print ("Fast Command Processor configured: ", configured)

    def set_start_trigger(self):
        self.ipb_write("usr.ctrl.fast_command_block.control.start_trigger",1,1)
        sleep(0.2)
        #start_trigger = self.ipb_read("usr.ctrl.fast_command_block.control.start_trigger",1)
        #print "Start trigger: ", start_trigger

    # reading counter values
    def status_fcp(self):
        configured = self.ipb_read("usr.stat.fast_command_block.general.configured",1)
        fsm_state = self.ipb_read("usr.stat.fast_command_block.general.fsm_state",1)
        trg_source = self.ipb_read("usr.stat.fast_command_block.general.source",1)
        trg_counter = self.ipb_read("usr.stat.fast_command_block.trigger_in_counter",1)
        print ("Fast Command Processor status ...")
        print ("------------------------------------------------")
        print ("configured                ", configured)
        print ("fsm_state                 ", fsm_state)
        print ("trg_source                ", trg_source)
        print ("trg_counter               ", trg_counter)
        print ("------------------------------------------------")
        print ("")

################################################################
### TTS decoder
################################################################
    
    def set_tts_fsm_enable(self):
        self.ipb_write("usr.cnfg.amc13.sw_tts_state_valid",0,1)
        TTS_FSM = self.ipb_read("usr.cnfg.amc13.sw_tts_state_valid",1)
        print ("TTS state valid: ", TTS_FSM)

    def set_tts_fsm_disable(self):
        self.ipb_write("usr.cnfg.amc13.sw_tts_state_valid",1,1)
        TTS_FSM = self.ipb_read("usr.cnfg.amc13.sw_tts_state_valid",1)
        print ("TTS state valid: ", TTS_FSM)

    def read_tts_state(self):
        TTS_state = self.ipb_read("usr.cnfg.amc13.sw_tts_state",1)
        print ("Current TTS state: ", TTS_state)
        return TTS_state

    def set_tts_state(self, bit):
        self.ipb_write("usr.cnfg.amc13.sw_tts_state",bit,1)

    def set_ipbus_any_buffer_full(self, bit):
        self.ipb_write("usr.cnfg.tts.ipbus_any_buffer_full",bit,1)
        ipbus_any_buffer_full = self.ipb_read("usr.cnfg.tts.ipbus_any_buffer_full",1)
        print ("ipbus_any_buffer_full: ", ipbus_any_buffer_full)
        return ipbus_any_buffer_full

    def set_ipbus_all_buffer_empty(self, bit):
        self.ipb_write("usr.cnfg.tts.ipbus_all_buffer_empty",bit,1)
        ipbus_all_buffer_empty = self.ipb_read("usr.cnfg.tts.ipbus_all_buffer_empty",1)
        print ("ipbus_all_buffer_empty: ", ipbus_all_buffer_empty)
        return ipbus_all_buffer_empty

################################################################
################################################################
################################################################



################################################################
