# pyOT

Python interface for the part of the OT-uDTC firmware that is developed by Wigner

# Customization for the CERN setup 

Find IP address here: https://cms-tracker-daq.web.cern.ch/cms-tracker-daq/components/200_fc7_crate_186/
Update IP address in ot.py line 25: uTTC = pyOT( "192.168.4.10" )

# Request changes by emailing to balazs.tamas@wigner.hu or veszpremi.viktor@wigner.hu


