export LD_LIBRARY_PATH=/opt/cactus/lib:$LD_LIBRARY_PATH
export PATH=/opt/cactus/bin:$PATH:./
# python version
alias python='/usr/bin/python3' # CC7

export IP_Addr_Readout_FC7s=(192.168.4.10 192.168.4.30)
export pyot_Dir=$(pwd)

ot() {
    ip=`echo $1 | sed -n 's/^\([^0-9]*\)\([0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)\(.*\)$/\2/p'`
    if [ "$ip" == "" ]; then
	echo "Usage: ot <IP address> <command(s)>"
	echo "Running command $* on 0.0.0.0"
	IPOT="0.0.0.0" python $pyot_Dir/ot.py $*
    else
	dir=$(pwd)
	cd $pyot_Dir
	shift
	echo "Running command $* on $ip"
	IPOT="$ip" python $pyot_Dir/ot.py $*
	cd $dir
    fi
}

ots() {
    for i in "${!IP_Addr_Readout_FC7s[@]}"; do
	ip="${IP_Addr_Readout_FC7s[$i]}"
	ot $ip $*
    done
}
